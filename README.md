# Data agnostic mapper
Project to take a data file in xml format and a mapping file to output mapped data in json format

## Running the code

### Prerequisites 
* Java
* Maven

### Through the shell
```
$ mvn install
...
$ java -jar target/arbor-data-mapper-1.0-SNAPSHOT-jar-with-dependencies.jar data/data.xml data/rules.json
[{"patientid":"1234","sex":"male","name":"John Smith","state":"MI","age":"59"},{"patientid":"5678","sex":"female","name":"Jane Smith","state":"OH","age":"49"}]
```

### With an IDE
1. Import the project using the pom.xml file
2. Navigate to the main `App` class
3. Use the IDE's run configuration to provide 2 arguments: path to data file and path to mapping file
4. Run the `main` method

## Mapping file format
The mapping file is in a json format that contains mapping rules for the xml data fields and includes the new field name and the name of a mapping function in `com.arbor.MapperFunction`. 
```json
{
  "originalXmlFieldName": {
    "fieldName": "newFieldName",
    "mapper": "mappingFunction"
  },
  "originalXmlFieldName": {
    "fieldName": "newFieldName",
    "mapper": "mappingFunction"
  }
}
```

## Mapping Functions
Reusable mapping functions are defined in `com.arbor.MapperFunction`.

Currently supported mappers:
* IDENTITY
* ABBREVIATE_STATE
* DATE_TO_AGE
* GENDER_TO_SEX

## Future Improvements
* Decouple mapping and serialization steps 
* Handle arbitrarily nested xml inputs
* Tests