package com.arbor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

/**
 * Simple class to read in an xml data file and rules json file and print out a transformed json output
 */
public class App {

    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("App requires 2 arguments: data file path and mapping file path");
            return;
        }

        String dataFilePath = args[0];
        String mappingFilePath = args[1];

        try {
            DataInput input = readDataInputFile(dataFilePath);
            Map<String, Rule> mappingRules = readMappingRulesFile(mappingFilePath);
            System.out.println(mapToJson(input, mappingRules));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String mapToJson(DataInput input, Map<String, Rule> mappingRules) throws JsonProcessingException {
        SimpleModule module = new SimpleModule();
        module.addSerializer(DataInput.class, new MappingSerializer(DataInput.class, mappingRules));

        ObjectMapper mapper = new JsonMapper();
        mapper.registerModule(module);
        return mapper.writeValueAsString(input);
    }

    private static Map<String, Rule> readMappingRulesFile(String mappingFilePath) throws IOException {
        ObjectMapper mapper = new JsonMapper();
        File file = Paths.get(mappingFilePath).toFile();
        TypeReference<Map<String, Rule>> typeRef = new TypeReference<Map<String, Rule>>() {};
        return mapper.readValue(file, typeRef);
    }

    private static DataInput readDataInputFile(String dataFilePath) throws IOException {
        XmlMapper mapper = new XmlMapper();
        File file = Paths.get(dataFilePath).toFile();
        TypeReference<List<Map<String, String>>> inputDataTypeRef = new TypeReference<List<Map<String, String>>>() {};
        List<Map<String, String>> input = mapper.readValue(file, inputDataTypeRef);
        return new DataInput().setData(input);
    }
}
