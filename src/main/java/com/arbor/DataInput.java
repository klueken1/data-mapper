package com.arbor;

import java.util.List;
import java.util.Map;

/**
 * Model class to hold generic data
 */
public class DataInput {

    private List<Map<String, String>> data;

    public List<Map<String, String>> getData() {
        return data;
    }

    public DataInput setData(List<Map<String, String>> data) {
        this.data = data;
        return this;
    }
}
