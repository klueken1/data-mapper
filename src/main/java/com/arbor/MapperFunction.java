package com.arbor;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

/**
 * Reusable mapping functions for integrations
 */
public enum MapperFunction {
    IDENTITY((input) -> input),
    ABBREVIATE_STATE(abbreviateState()),
    DATE_TO_AGE(dateToAge()),
    GENDER_TO_SEX((input) -> input.equals("m") ? "male" : "female");

    private Function<String, String> function;

    Function<String, String> getFunction() {
        return this.function;
    }

    MapperFunction(Function<String, String> function) {
        this.function = function;
    }

    /**
     * Stubbed out sample implementation. Would need to map all 50 states, DC, and possibly US territories
     *
     * @return a mapping function to abbreviate state names
     */
    private static Function<String, String> abbreviateState() {
        return (input) -> {
          switch (input) {
              case "Michigan" :
                  return "MI";
              case "Ohio" :
                  return "OH";
              default:
                  return "NY"; // not a reasonable default, would need fixed
          }
        };
    }

    private static Function<String, String> dateToAge() {
        return (input) -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            LocalDate birthDate = LocalDate.parse(input, formatter);
            int age = Period.between(birthDate, LocalDate.now()).getYears();
            return String.valueOf(age);
        };
    }


}
