package com.arbor;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Map;

/**
 * Extension of Jackson's {@link StdSerializer} that takes a map of rules to transform the original
 * field names and values
 */
public class MappingSerializer extends StdSerializer<DataInput> {

    private Map<String, Rule> mappingRules;

    public MappingSerializer(Class<DataInput> t, Map<String, Rule> mappingRules) {
        super(t);
        this.mappingRules = mappingRules;
    }

    @Override
    public void serialize(DataInput input, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartArray();
        for (Map<String, String> fields : input.getData()) {
            jsonGenerator.writeStartObject();
            for (String field : fields.keySet()) {
                Rule rule = mappingRules.get(field);
                String transformedValue = rule.getMapper().getFunction().apply(fields.get(field));
                jsonGenerator.writeStringField(rule.getFieldName(), transformedValue);
            }
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
    }
}
