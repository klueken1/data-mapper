package com.arbor;

/**
 * Model class to represent a new field name and data mapping function for transforming data
 */
public class Rule {
    String fieldName;
    MapperFunction mapper;

    public String getFieldName() {
        return fieldName;
    }

    public Rule setFieldName(String fieldName) {
        this.fieldName = fieldName;
        return this;
    }

    public MapperFunction getMapper() {
        return mapper;
    }

    public Rule setMapper(MapperFunction mapper) {
        this.mapper = mapper;
        return this;
    }
}
